# InitWorld.gd

extends Node2D

const Floor := preload("res://sprite/Floor.tscn")
const Wall := preload("res://sprite/Wall.tscn")
const ArrowX := preload("res://sprite/ArrowX.tscn")
const ArrowY := preload("res://sprite/ArrowY.tscn")
const Player := preload("res://sprite/PC.tscn")
const Dwarf := preload("res://sprite/Dwarf.tscn")

var _new_GroupName := preload("res://library/GroupName.gd").new()
var _new_ConvertCoord := preload("res://library/ConvertCoord.gd").new()
var _new_DungeonSize := preload("res://library/DungeonSize.gd").new()



func _ready() -> void:

	_createFloor()
	_createWall()
	_createArrows()
	_createPlayer()
	_createDwarf()



func _createFloor() -> void:

	for x in range(_new_DungeonSize.MAX_X):
		for y in range(_new_DungeonSize.MAX_Y):
			_createSprite(Floor, _new_GroupName.FLOOR, x, y)



func _createWall() -> void:

	for x in range(8, 13):
		for y in range(5, 10):
			_createSprite(Wall, _new_GroupName.WALL, x, y)



func _createArrows() -> void:

	_createSprite(ArrowX, _new_GroupName.ARROW, 0, 12,
			-_new_DungeonSize.ARROW_MARGIN, 0)
	_createSprite(ArrowY, _new_GroupName.ARROW, 5, 0,
			0, -_new_DungeonSize.ARROW_MARGIN)



func _createPlayer() -> void:

	_createSprite(Player, _new_GroupName.PC, 0, 0)



func _createDwarf() -> void:

	_createSprite(Dwarf, _new_GroupName.DWARF, 3, 3)
	_createSprite(Dwarf, _new_GroupName.DWARF, 14, 5)
	_createSprite(Dwarf, _new_GroupName.DWARF, 7, 11)



func _createSprite( prefab: PackedScene, group: String,
	x: int, y: int, x_offset: int = 0, y_offset: int = 0) -> void:

	var new_sprite:Sprite = prefab.instance() as Sprite
	new_sprite.position = _new_ConvertCoord.index_to_vector(x, y, x_offset, y_offset)
	new_sprite.add_to_group(group)

	add_child(new_sprite)
